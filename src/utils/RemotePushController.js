import React, {usestate, useEffect} from 'react';
import PushNotification, {Importance} from "react-native-push-notification";


const RemotePushController = () => {
    useEffect(() => {
        PushNotification.configure({
            onRegister: function (token) {
                console.log("TOKEN:", token);
                },
            
                onNotification: function (notification) {
                    console.log("LOCAL NOTIFICATION:", notification);
                },
                onAction: function (notification) {
                    console.log("ACTION:", notification.action);
                    console.log("NOTIFICATION:", notification);
                },
            
                onRegistrationError: function(err) {
                    console.error(err.message, err);
                },
                senderID: '1042206657472',
                popInitialNotification: true,
                requestPermissions: true,
            });
    
    
    }, [])


    return null;
    
}

export default RemotePushController;