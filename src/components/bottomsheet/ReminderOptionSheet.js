import React, { useState, useEffect, useRef } from 'react';
import { SafeAreaView, Text, View, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, useColorScheme, } from 'react-native';

import { media } from '../../global/media';
import { colors } from '../../global/colors';
import { fontSize, Poppins } from '../../global/fontFamily';

import Icon from '../../utils/icons';

import RBSheet from "react-native-raw-bottom-sheet";

const ReminderOptionSheet = ({refRBSheet, openDeleteModal, openEditSheet}) => {

    const [showTaskModal, setShowTaskModal] = useState(false);

    return (
        <View>
            <RBSheet
                ref={refRBSheet}
                closeOnDragDown={true}
                closeOnPressMask={true}
                dragFromTopOnly={true}
                height={200}
                onClose={() => {}}
                customStyles={{
                    wrapper: {
                        backgroundColor: "#00000090",
                    },
                    draggableIcon: {
                        backgroundColor: colors.gray,
                        width: 100,
                    }, 
                    container: {
                        backgroundColor: 'white',
                        borderTopRightRadius: 20,
                        borderTopLeftRadius: 20,
                        paddingTop: 10,
                    }
                }}
            >
                <View style={styles.reminderSheetContainer} >
                    <TouchableOpacity
                        activeOpacity={0.5}
                        onPress={() => {
                            refRBSheet.current.close();
                            openEditSheet();
                        }}
                        style={[styles.reminderOption, {marginBottom: 15}]}
                    >
                        <Icon type={'AntDesign'} name={'edit'}  size={26} color={colors.black} />
                        <Text style={styles.reminderOptionTitle} >Edit Reminder</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        activeOpacity={0.5}
                        onPress={() => {
                            refRBSheet.current.close();
                            openDeleteModal();
                        }}
                        style={styles.reminderOption}
                    >
                        <Icon type={'Feather'} name={'trash-2'}  size={26} color={colors.black} />
                        <Text style={styles.reminderOptionTitle} >Delete Reminder</Text>
                    </TouchableOpacity>
                </View>
            </RBSheet>
        </View>
    )
}

const styles = StyleSheet.create({
    reminderSheetContainer: {
        flex: 1, 
        padding: 20,
    },
    reminderOption: {
        flexDirection: 'row',
        alignItems: 'center',
        padding: 10,
        borderRadius: 8,
        backgroundColor: colors.light_gray,
    },
    reminderOptionTitle: {
        fontSize: 18,
        fontFamily: Poppins.Medium,
        color: colors.black,
        marginLeft: 12,
    }
})

export default ReminderOptionSheet
