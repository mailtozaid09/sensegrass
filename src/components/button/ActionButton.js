import React from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, } from 'react-native'

import { colors } from '../../global/colors';
import Icon from '../../utils/icons';


const ActionButton = ({title, onPress, type}) => {
       
    
    return (
        <View style={styles.container} >
            <TouchableOpacity 
                activeOpacity={0.5}
                onPress={onPress} 
                style={styles.button} >
                    <Icon type={'Feather'} name={'plus'}  size={30} color={colors.white} />
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        position: 'absolute', 
        bottom: 20, 
        right: 20,
    },
    button: {
        height: 60, 
        width: 60,
        borderRadius: 30,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.primary,
    },
})

export default ActionButton