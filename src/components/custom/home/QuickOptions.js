import React, {useState} from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, FlatList, } from 'react-native'

import Icon from '../../../utils/icons';

import { media } from '../../../global/media';
import { colors } from '../../../global/colors';
import { Poppins } from '../../../global/fontFamily';
import { screenWidth } from '../../../global/constants';

import { useNavigation } from '@react-navigation/native';

const QuickOptions = ({userDetails}) => {

    const navigation = useNavigation();



    const quickOptions = [
        {
            title: 'Bank\nTransfer',
            iconName: 'bank',
            iconType: 'FontAwesome',
        },
        {
            title: 'Scan\nQR Code',
            iconName: 'qrcode-scan',
            iconType: 'MaterialCommunityIcons',
        },
        {
            title: 'UPI\nTransfer',
            iconName: 'at-sign',
            iconType: 'Feather',
        },
        {
            title: 'View\nExpenses',
            iconName: 'document-text',
            iconType: 'Ionicons',
        },
    ]
    
    return (
        <View style={styles.container} >
            <FlatList
                showsHorizontalScrollIndicator={false}
                data={quickOptions}
                contentContainerStyle={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}
                keyExtractor={item => item.title}
                renderItem={({item, index}) => (
                    <TouchableOpacity 
                        activeOpacity={0.5} 
                        onPress={() => {}}
                        style={styles.optionContainer} key={index} >
                        <View style={styles.iconContainer} >
                            <Icon type={item.iconType} name={item.iconName}  size={26} color={colors.black} />
                        </View>
                        <Text style={styles.title} >{item.title}</Text>
                    </TouchableOpacity>
                )}
            />
        </View>
    )
}

const styles =  StyleSheet.create({
    container: {
        marginTop: 30,
        width: '100%',
        flexDirection: 'row', 
        alignItems: 'center', 
        justifyContent: 'space-between',
    },
    iconContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.white,
        borderColor: colors.gray,
        borderRadius: 8,
        borderWidth: 0.5,
        height: 60,
        width: 60,
        //paddingVertical: 16,
        marginBottom: 10,
    },
    optionContainer: {
        marginRight: 0,
        borderRadius: 4,
    },
    title: {
        fontSize: 14,
        fontFamily: Poppins.SemiBold,
        color: colors.black,
        textAlign: 'center'
    },
})

export default QuickOptions