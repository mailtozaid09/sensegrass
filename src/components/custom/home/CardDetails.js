import React, {useState} from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, FlatList, } from 'react-native'

import Icon from '../../../utils/icons';

import { media } from '../../../global/media';
import { colors } from '../../../global/colors';
import { Poppins } from '../../../global/fontFamily';
import { screenWidth } from '../../../global/constants';

import { useNavigation } from '@react-navigation/native';

import LinearGradient from 'react-native-linear-gradient';


const CardDetails = ({showToastMsg}) => {

    const navigation = useNavigation();

    const walletBalance = '321,500.00';
    const upiID = '2121333456@farmaxis';


    return (
        <LinearGradient 
            angle={120}
            useAngle={true}
            colors={['#10B247', '#0E742A']}
            style={styles.container} 
        >
            <LinearGradient 
                angle={120}
                useAngle={true}
                colors={['#68EF96', '#24843E']}
                style={styles.balanceContainer} >
                <Text style={styles.walletTitle} >Wallet Balance</Text>
                <Text style={styles.walletSubTitle} >{'\u20B9'} {walletBalance}</Text>
            </LinearGradient>
            <View style={styles.upiContainer} >
                <Text style={styles.upiTitle} >UPI ID: {upiID}</Text>
                <TouchableOpacity activeOpacity={0.5} onPress={() => showToastMsg(upiID)} >
                    <Icon type={'Ionicons'} name={'copy-outline'}  size={22} color={colors.black} />
                </TouchableOpacity>
            </View>
            <TouchableOpacity activeOpacity={0.5} style={styles.buttonContainer} >
                <Text style={styles.title} >Add Money</Text>
                <Icon type={'Entypo'} name={'chevron-small-right'}  size={28} color={colors.white} />
            </TouchableOpacity>
        </LinearGradient>
    )
}

const styles =  StyleSheet.create({
    container: {
        marginTop: 15,
        width: '100%',
        justifyContent: 'center', 
        backgroundColor: 'red',
        borderRadius: 16,
        padding: 20,
    },
    title: {
        fontSize: 14,
        fontFamily: Poppins.SemiBold,
        color: colors.white,
        marginLeft: 8,
    },
    upiTitle: {
        fontSize: 15,
        fontFamily: Poppins.SemiBold,
        color: colors.black,
        marginRight: 10,
    },
    walletTitle: {
        fontSize: 14,
        fontFamily: Poppins.SemiBold,
        color: colors.black,
    },
    walletSubTitle: {
        fontSize: 24,
        lineHeight: 28,
        fontFamily: Poppins.SemiBold,
        color: colors.black,
    },
    balanceContainer: {
        backgroundColor: 'green',
        padding: 10,
        marginBottom: 10,
        borderRadius: 10,
    },
    upiContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 6,
    },
    buttonContainer: {
        padding: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-evenly',
        backgroundColor: colors.black,
        width: 140,
        borderRadius: 50,
    },
})

export default CardDetails