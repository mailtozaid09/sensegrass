import React, {useState} from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, FlatList, } from 'react-native'

import Icon from '../../../utils/icons';

import { media } from '../../../global/media';
import { colors } from '../../../global/colors';
import { Poppins } from '../../../global/fontFamily';
import { screenWidth } from '../../../global/constants';

import { useNavigation } from '@react-navigation/native';

const FilterOptions = ({userDetails}) => {

    const navigation = useNavigation();
    const [currentOption, setCurrentOption] = useState('Account');


    const filterOptions = ['Account', 'Debit Card', 'Loans']
    
    return (
        <View style={styles.container} >
            <FlatList
                horizontal
                showsHorizontalScrollIndicator={false}
                data={filterOptions}
                keyExtractor={item => item}
                renderItem={({item, index}) => (
                    <TouchableOpacity 
                        activeOpacity={0.5} 
                        onPress={() => {setCurrentOption(item)}}
                        style={[styles.filterContainer, {backgroundColor: item == currentOption ? colors.black : colors.white}]}  key={index}  >
                        <Text style={[styles.title, {color: item == currentOption ? colors.white : colors.black}]} >{item}</Text>
                    </TouchableOpacity>
                )}
            />
        </View>
    )
}

const styles =  StyleSheet.create({
    container: {
        marginTop: 15,
        width: '100%',
        flexDirection: 'row', 
        alignItems: 'center', 
        justifyContent: 'space-between',
    },
    filterContainer: {
        marginRight: 12,
        padding: 6,
        paddingHorizontal: 12,
        borderWidth: 1,
        borderRadius: 4,
    },
    title: {
        fontSize: 14,
        fontFamily: Poppins.SemiBold,
        color: colors.black,
    },
})

export default FilterOptions