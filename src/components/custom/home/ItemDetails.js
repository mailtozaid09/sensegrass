import React, {useState} from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, FlatList, } from 'react-native'

import Icon from '../../../utils/icons';

import { media } from '../../../global/media';
import { colors } from '../../../global/colors';
import { Poppins } from '../../../global/fontFamily';
import { screenWidth } from '../../../global/constants';

import { useNavigation } from '@react-navigation/native';


const ItemDetails = ({title, dataArr, onViewAll}) => {

    const navigation = useNavigation();
  
    
    return (
        <View style={styles.container} >
            <View style={styles.titleContainer} >
                <Text style={styles.title}>{title}</Text>
                <TouchableOpacity activeOpacity={0.5} onPress={onViewAll} >
                    <Text style={styles.viewAllText} >View all</Text>
                </TouchableOpacity>
            </View>
            <FlatList
                showsHorizontalScrollIndicator={false}
                data={dataArr.slice(0,5)}
                contentContainerStyle={{borderTopWidth: 0.5, borderBottomWidth: 0.5, borderColor: colors.gray}}
                keyExtractor={item => item.transaction_id}
                renderItem={({item,index}) => (
                    <View style={[styles.itemContainer, {borderBottomWidth: dataArr?.length-1 > index ? 0.1 : 0}]} key={index} >
                        <View style={styles.userContainer} >
                            <View style={styles.userNameContainer} > 
                                <Text style={styles.userNameIcon} >{(item.user_name).charAt(0)}</Text>
                            </View>
                            <View style={{flex: 1}} >
                                <Text style={styles.userName} >{item.user_name}</Text>
                                <View>
                                    <Text numberOfLines={1} style={styles.time}>{item.time}, {item.bank_name}</Text>
                                </View>
                            </View>
                        </View>
                        {title == 'Payments Pending'
                        ?
                        <TouchableOpacity activeOpacity={0.5} style={styles.sendMoneyContainer} >
                            <Text style={[styles.title, {fontSize: 12,}]}>Send Money</Text>
                        </TouchableOpacity>
                        :
                        <View>
                            <Text style={[styles.amount, {color: item.transaction_type == 'Debit' ? colors.primary : colors.reddish}]}>{item.transaction_type == 'Debit' ? '+' : '-' }{'\u20B9'}{item.amount}</Text>
                        </View>
                        }
                    </View>
                )}
            />
        </View>
    )
}

const styles =  StyleSheet.create({
    container: {
        marginTop: 25,
        width: screenWidth,
    },
    userContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        flex: 1,
        marginRight: 10,
    },
    title: {
        fontSize: 16,
        fontFamily: Poppins.SemiBold,
        color: colors.black,
    },
    titleContainer: {
        marginBottom: 8,
        paddingHorizontal: 20,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    viewAllText: {
        fontSize: 14,
        fontFamily: Poppins.Medium,
        color: colors.black,
        textDecorationLine: 'underline'
    },
    itemContainer: {
        borderRadius: 4,
        paddingHorizontal: 20,
        paddingVertical: 12,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: colors.white,
        justifyContent: 'space-between'
    },
    userNameContainer: {
        height: 50,
        width: 50,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 25,
        marginRight: 10,
        backgroundColor: colors.primary,
    },
    userNameIcon: {
        fontSize: 22,
        fontFamily: Poppins.Medium,
        color: colors.black,
    },
    userName: {
        fontSize: 16,
        fontFamily: Poppins.SemiBold,
        color: colors.black,
    },
    time: {
        fontSize: 14,
        fontFamily: Poppins.Medium,
        color: colors.black,
    },
    amount: {
        fontSize: 16,
        fontFamily: Poppins.SemiBold,
        color: colors.black,
    },
    sendMoneyContainer: {
        borderWidth: 1,
        padding: 10,
        paddingVertical: 8,
        borderRadius: 6,
    }
})

export default ItemDetails