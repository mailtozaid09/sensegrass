import { 
	ADD_REMINDER_DETAILS, EDIT_REMINDER_DETAILS, DELETE_REMINDER_DETAILS, 
	ADD_TASK_DETAILS, EDIT_TASK_DETAILS, DELETE_TASK_DETAILS,
} from './actionTypes';

let nextReminderId = 0;
let nextTaskId = 0

export function addReminderDetails(params) {
    return {
        type: ADD_REMINDER_DETAILS,
        payload: {
            id: ++nextReminderId,
			reminderName: params.reminderName,
			reminderDay: params.reminderDay,
			reminderTime: params.reminderTime,
			reminderType: params.reminderType,
        },
    };
};

export const editReminderDetails = params => {
	return {
		type: EDIT_REMINDER_DETAILS,
		payload: {
			id: params.id,
			reminderName: params.reminderName,
			reminderDay: params.reminderDay,
			reminderTime: params.reminderTime,
			reminderType: params.reminderType,
		},
	};
};

export const deleteReminderDetails = id => {
  return {
		type: DELETE_REMINDER_DETAILS,
		payload: {
			id
		},
	};
};



export function addTaskDetails(params) {
    return {
        type: ADD_TASK_DETAILS,
        payload: {
            id: ++nextTaskId,
			taskName: params.taskName, 
			taskDescription: params.taskDescription,
			taskDate: params.taskDate, 
			taskTime: params.taskTime,
			tripId: params.tripId,
			taskDay: params.taskDay,
			isTaskDone: params.isTaskDone,
        },
    };
};

export const editTaskDetails = params => {
	return {
		type: EDIT_TASK_DETAILS,
		payload: {
			id: params.id,
			taskName: params.taskName, 
			taskDescription: params.taskDescription,
			taskDate: params.taskDate, 
			taskTime: params.taskTime,
			tripId: params.tripId,
			taskDay: params.taskDay,
			isTaskDone: params.isTaskDone,
		},
	};
};

export const deleteTaskDetails = id => {
  return {
		type: DELETE_TASK_DETAILS,
		payload: {
			id
		},
	};
};



export function screenLoader(screen_loader) {
	return {
		type: '@home/SCREEN_LOADER',
		payload: {
			screen_loader,
		},
	};
}
