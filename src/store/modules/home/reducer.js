//import { dummyCategories, dummyExpenses } from "../../../global/sampleData";
import { 
    ADD_REMINDER_DETAILS, ADD_TASK_DETAILS, 
    DELETE_REMINDER_DETAILS, DELETE_TASK_DETAILS, 
    EDIT_REMINDER_DETAILS, EDIT_TASK_DETAILS 
} from "./actionTypes";

const INITIAL_STATE = {
    screen_loader: false,
    reminders_list: [],
    task_list: [],
};

export default function home(state = INITIAL_STATE, action) {
    switch (action.type) {

    case ADD_REMINDER_DETAILS: {
        const {
            id, reminderName, reminderDay, reminderTime, reminderType,
        } = action.payload
        return {
            ...state,
            reminders_list: [
                ...state.reminders_list, {
                    id, reminderName, reminderDay, reminderTime, reminderType,
                }
            ]
        }
    };


    case EDIT_REMINDER_DETAILS:{
        const { reminderName, reminderDay, reminderTime, reminderType, } = action.payload
        const updatedReminder = state.reminders_list.map(reminder => {
        if (reminder.id != action.payload.id) {
            return reminder;
        } else {
            return {
            ...reminder,
                reminderName: reminderName,
                reminderDay: reminderDay,
                reminderTime: reminderTime,
                reminderType: reminderType,
            };
        }
        });
    
    return {
        ...state,
        reminders_list: updatedReminder
    };}

    case DELETE_REMINDER_DETAILS: {
        const { id } = action.payload
            return {
                ...state,
                reminders_list: state.reminders_list.filter((reminder) => reminder.id != id)
        };
    }


    case ADD_TASK_DETAILS: {
        const {
            
            id, taskName, taskDescription, taskDate, taskDay, taskTime, tripId, isTaskDone,
        } = action.payload
        return {
            ...state,
            task_list: [
                ...state.task_list, {
                    id, taskName, taskDescription, taskDate, taskDay, taskTime, tripId, isTaskDone, 
                }
            ]
        }
    };


    case EDIT_TASK_DETAILS:{
        const { taskName, taskDescription, taskDate, taskDay, taskTime, tripId, isTaskDone, } = action.payload
        const updatedTask = state.task_list.map(task => {
        if (task.id != action.payload.id) {
            return task;
        } else {
            return {
            ...task,
                taskName: taskName, 
                taskDescription: taskDescription,
                taskDate: taskDate, 
                taskTime: taskTime,
                taskDay: taskDay,
                tripId: tripId,
                isTaskDone: isTaskDone,
            };
        }
        });
    
    return {
        ...state,
        task_list: updatedTask
    };}

    case DELETE_TASK_DETAILS: {
        const { id } = action.payload
            return {
                ...state,
                task_list: state.task_list.filter((task) => task.id != id)
        };
    }




    
    case '@home/SCREEN_LOADER':
        return {
            ...state,
            screen_loader: action.payload.screen_loader,
        };
        default:
        return state;
    }
}
