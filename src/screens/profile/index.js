import React, {useState, useEffect} from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, } from 'react-native'

import { colors } from '../../global/colors'
import { Poppins } from '../../global/fontFamily'
import { media } from '../../global/media'

import { userLoggedIn } from '../../store/modules/auth/actions'
import { useDispatch } from 'react-redux'
import auth from '@react-native-firebase/auth';
import AlertModal from '../../components/modal/AlertModal'
import { LocalNotification } from '../../utils/LocalPushController'

const ProfileScreen = ({navigation}) => {

    const dispatch = useDispatch();
    const [userDetails, setUserDetails] = useState({});
    
    const [showAlertModal, setShowAlertModal] = useState(false);

    const [alertType, setAlertType] = useState('');
    const [alertTile, setAlertTile] = useState('');
    const [alertDescription, setAlertDescription] = useState('');
    
        
    const profile_options = [
        {
            title: 'Dashboard',
            icon: media.home_active,
        },
        {
            title: 'Reminders',
            icon: media.reminder_active,
        },
        {
            title: 'Logout',
            icon: media.logout,
        },
    ]

    const getUserDetails = () => {     
    //     auth().onAuthStateChanged((user) => {
    //         if (user) {
    //             setUserDetails(user)
    //         }
    //   });
    }

    useEffect(() => {
        getUserDetails()
    }, [])
    

    const onProfileOptions = (option) => {

        if(option == 'Dashboard'){
            navigation.navigate('HomeStack')
        }else if(option == 'Reminders'){
            navigation.navigate('RemindersStack')
        }else if(option == 'Logout'){
            setAlertType('Logout')
            setAlertTile("Are you sure!");
            setAlertDescription("You want to logout?");
            setShowAlertModal(true)
        }
        
    }


    const logoutFunction = () => {
        auth().signOut()
        .then(() => {
            LocalNotification({
                title: "You've been logged out successfully.!",
                message: `Thank you for using Sensegrass`,
            })
            
            console.log("successfully logged out");
            
            dispatch(userLoggedIn(false))
            navigation.navigate('LoginStack', {screen: 'Onboarding'})
        })
        .catch(error => {
            console.log("error logging out" + error);
        });
    }
    
    return (
        <SafeAreaView style={styles.container} >
           
                
                <View style={styles.mainContainer} >
                    <View style={styles.imageIconContainer}>
                        <Image source={media.user_active} style={{height: 50, width: 50}} />
                    </View>
                    
                    <View style={{alignItems: 'center'}} >
                        <Text style={styles.title} >Sensegrass</Text>
                        <Text style={styles.subtitle} >{userDetails?.email}</Text>
                    </View>


                    <View style={styles.profileOptions} >
                        {profile_options?.map((item, index) => (
                            <TouchableOpacity  
                                key={index}
                                activeOpacity={0.5} 
                                onPress={() => {onProfileOptions(item.title)}}
                                style={[styles.profileOptionsContainer, item.title == 'Logout' && {borderBottomWidth: 0}]} 
                            >
                                <View style={[styles.optionIconContainer, item.title == 'Logout' && {backgroundColor: colors.red,}]} >
                                    <Image source={item.icon} style={{height: 28, width: 28, }} />
                                </View>
                                <Text style={styles.optionTitle} >{item.title}</Text>
                            </TouchableOpacity>
                        ))}
                    </View>
                </View>

                {showAlertModal && 
                    <AlertModal
                        alertType={alertType}
                        title={alertTile}
                        description={alertDescription}
                        onLogout={() => {
                            setShowAlertModal(false); 
                            logoutFunction();
                        }}
                        closeModal={() => {setShowAlertModal(false); }} 
                    />
                }
          
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.primary,
    },
    mainContainer: {
        padding: 20,
        flex: 1,
        marginTop: 100,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        alignItems: 'center', 
        //justifyContent: 'space-between',
        backgroundColor: colors.white,
    },
    title: {
        marginTop: 50,
        fontSize: 18, 
        fontFamily: Poppins.Medium,
        color: colors.black,
    },
    subtitle: {
        fontSize: 16, 
        lineHeight: 20,
        fontFamily: Poppins.Medium,
        color: colors.black,
    },
    imageIconContainer: {
        height: 100,
        width: 100,
        borderRadius: 50,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1,
        borderColor: colors.primary,
        backgroundColor: colors.white,
        position: 'absolute',
        top: -50,
    },
    optionTitle: {
        color: colors.black,
        fontSize: 16,
        fontFamily: Poppins.Medium
    },
    profileOptions: {
        width: '100%',
        borderRadius: 10,
        marginTop: 30,
        backgroundColor: colors.white,
    },
    profileOptionsContainer: {
        alignItems: 'center',
        flexDirection: 'row',
        paddingVertical: 12,
        borderBottomWidth: 1,
        borderColor: colors.primary,
    },
    optionIconContainer: {
        height: 50,
        width: 50,
        borderRadius: 25,
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 15,
        backgroundColor: colors.primary_light,
    }
})

export default ProfileScreen