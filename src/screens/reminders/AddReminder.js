import React, {useState, useEffect} from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, BackHandler, FlatList, } from 'react-native'

import { media } from '../../global/media'
import { colors } from '../../global/colors'
import { Poppins } from '../../global/fontFamily'

import Icon from '../../utils/icons'

import Input from '../../components/input'
import PrimaryButton from '../../components/button/PrimaryButton'

import CheckBox from '@react-native-community/checkbox'
import DatePicker from 'react-native-date-picker'
import moment from 'moment'
import { getTodaysDay } from '../../global/constants'
import { useDispatch } from 'react-redux'
import { addReminderDetails } from '../../store/modules/home/actions'
import { LocalNotification, ScheduleLocalNotification } from '../../utils/LocalPushController'

const AddReminder = ({navigation}) => {

    const dispatch = useDispatch()
   
    const picture_url = 'https://images.unsplash.com/photo-1628692945421-21162c93a8a6?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8NXx8cmVtaW5kZXJ8ZW58MHx8MHx8fDA%3D&auto=format&fit=crop&w=800&q=60'

    const daysOptions = [
        'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat',
    ]

    const reminderOptions = [
        {
            title: 'Repeat',
            value: false,
        },
        {
            title: 'Play Once',
            value: false,
        },
        {
            title: 'Everyday',
            value: false,
        },
    ]


    const [errors, setErrors] = useState({})
    const [reminderName, setReminderName] = useState('');
    const [reminderType, setReminderType] = useState('Repeat');

    const [todaysDate, setTodaysDate] = useState(new Date());
    const [showDatePicker, setShowDatePicker] = useState(false);

    const [activeDay, setActiveDay] = useState('Mon');

    useEffect(() => {
        var day = getTodaysDay(todaysDate)
        setActiveDay(day)


    }, []);

    const setReminderFunction = () => {

        var isValid = true
                
        if(!reminderName){
            console.log("Please enter a valid reminder name");
            isValid = false
            setErrors((prev) => {
                return {...prev, reminderName: 'Please enter a valid reminder name'}
            })
        }
       
        if(isValid){
            dispatch(addReminderDetails({
                reminderName: reminderName,
                reminderDay: activeDay,
                reminderTime: (todaysDate).toString(),
                reminderType: reminderType,
            }))

            navigation.goBack()


            LocalNotification({
                title: "You're all set!",
                message: `Your reminder to take medicines is scheduled at ${moment(todaysDate).format('hh:mm A')}.`,
            })

            ScheduleLocalNotification({
                date: todaysDate,
                picture: picture_url,
                title: "Your Health Matters!",
                message: `It's time for your scheduled medication. Please take your medicine to stay on the path to wellness.`,
            })
        }
    }

    return (
        <SafeAreaView style={styles.container} >
            <View style={styles.mainContainer} >
                <View>
                    <Text style={styles.label} >Enter Reminder</Text>
                    <View >
                        <Input
                            value={reminderName}
                            error={errors.reminderName}
                            placeholder="Enter medicine name"
                            onChangeText={(text) => {setReminderName(text); setErrors({}); }}
                        />
                    </View>
                    
                    <Text style={[styles.label, {marginBottom: 0, marginTop: 10}]} >Select Day</Text>
                    <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}} >
                        {daysOptions?.map((item, index) => (
                            <TouchableOpacity 
                                key={index}
                                activeOpacity={0.5} 
                                onPress={() => {setActiveDay(item)}}
                                style={[styles.dayContainer, item == activeDay ? styles.activeDayContainer : null]} >
                                <Text style={[styles.dayStyle, {color: item == activeDay ? colors.white : colors.black}]} >{item}</Text>
                            </TouchableOpacity> 
                        ))}
                    </View>

                    
                    <View style={[styles.rowStyle, {borderBottomWidth: 1, marginBottom: 30, marginTop: 20, height: 50}]} >
                        <View style={{ alignItems: 'center', justifyContent: 'center', paddingTop: 10}} >
                            <Text style={[styles.label, {marginBottom: 0}]} >Set time</Text>
                        </View>
                        <TouchableOpacity
                            onPress={() => {setShowDatePicker(true)}}
                            style={{ flex: 1, alignItems: 'flex-end', padding: 10, borderRadius: 10, justifyContent: 'center'}} >
                            <Text style={styles.timeStyle} >{moment(todaysDate).format('hh:mm A')}</Text>
                        </TouchableOpacity>

                        <DatePicker
                            modal
                            date={todaysDate}
                            mode="time"
                            open={showDatePicker}
                            onConfirm={(date) => {
                                setShowDatePicker(false);
                                setTodaysDate(date);
                                
                            }}
                            onCancel={() => {
                                setShowDatePicker(false)
                            }}
                        />
                    </View>

                    <View>
                        {reminderOptions?.map((item, index) => (
                            <View style={styles.rowStyle} key={index} >
                                <Text style={styles.heading} >{item.title}</Text>
                                <CheckBox
                                    disabled={false}
                                    tintColors={{ true: colors.primary, false: colors.gray }}
                                    value={reminderType == item.title ? true : false}
                                    onValueChange={(newValue) => setReminderType(item.title)}
                                />
                            </View>
                        ))}
                    </View>

        
                </View>
                
                <PrimaryButton
                    title="Set Reminder" 
                    onPress={() => {
                        setReminderFunction()
                    }}
                />
            </View>

           
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.white,
    },
    mainContainer: {
        padding: 20,
        flex: 1,
        justifyContent: 'space-between'
    },
    label: {
        fontSize: 16,
        marginBottom: 10,
        color: colors.black,
        fontFamily: Poppins.Medium,
    },
    heading: {
        fontSize: 18,
        marginBottom: 15,
        color: colors.black,
        fontFamily: Poppins.Medium,
    },
    rowStyle: {
        alignItems: 'center',
        justifyContent: 'space-between',
        flexDirection: 'row'
    },
    timeStyle: {
        fontSize: 22, 
        fontFamily: Poppins.Medium, 
        color: colors.black,
    },
    activeDayContainer: {
        paddingHorizontal: 10,
        borderRadius: 4,
        backgroundColor: colors.primary,
    },
    dayContainer: {
        padding: 8,
        paddingVertical: 6,
    },
    dayStyle: {
        fontSize: 14, 
        fontFamily: Poppins.Medium, 
    },
})


export default AddReminder