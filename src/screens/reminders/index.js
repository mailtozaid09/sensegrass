import React, {useState, useEffect, useRef} from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, BackHandler, FlatList, } from 'react-native'

import { media } from '../../global/media'
import { colors } from '../../global/colors'
import { Poppins } from '../../global/fontFamily'
import Icon from '../../utils/icons'
import ActionButton from '../../components/button/ActionButton'
import { useDispatch, useSelector } from 'react-redux'
import moment from 'moment'
import ReminderOptionSheet from '../../components/bottomsheet/ReminderOptionSheet'
import AlertModal from '../../components/modal/AlertModal'
import { deleteReminderDetails } from '../../store/modules/home/actions'
import LottieView from 'lottie-react-native'
import { screenWidth } from '../../global/constants'


const RemindersScreen = ({navigation}) => {

    const dispatch = useDispatch();

    const refRBSheet = useRef();
    const [showAlertModal, setShowAlertModal] = useState(false);

    const [deleteID, setDeleteID] = useState(null);
    const [editItem, setEditItem] = useState(null);

    const [alertType, setAlertType] = useState('');
    const [alertTile, setAlertTile] = useState('');
    const [alertDescription, setAlertDescription] = useState('');
    

    const reminders_list = useSelector(state => state.home.reminders_list);

    useEffect(() => {
    }, []);

    const deleteFunction = () => {
        dispatch(deleteReminderDetails(deleteID))
    }
    
    return (
        <SafeAreaView style={styles.container} >
            <View style={styles.mainContainer} >
                
                <Text style={styles.title} >Scheduled reminders</Text>

      
                <FlatList
                    showsVerticalScrollIndicator={false}
                    data={reminders_list}
                    keyExtractor={item => item.id}
                    renderItem={({item, index}) => (
                        <View style={[styles.reminderContainer, {marginBottom: reminders_list?.length -1 <= index ? 70 : 10}]} key={item.id} >
                           <View style={styles.titleContainer} >
                                <View style={{height: 40, width: 40, borderRadius: 20, backgroundColor: colors.primary, alignItems: 'center', justifyContent: 'center'}} >
                                    <Icon type={'Fontisto'} name={'clock'}  size={22} color={colors.white} />
                                </View>
                                <Text numberOfLines={1} style={styles.reminderTitle} >{item.reminderName}</Text>
                            </View>
                            <View style={styles.rowStyle} >
                                <Text style={styles.reminderTime} >{moment(item.reminderTime).format('hh:mm A')}</Text>
                                <TouchableOpacity 
                                    activeOpacity={0.5}
                                    onPress={() => {
                                        refRBSheet.current.open();
                                        setDeleteID(item.id);
                                        setEditItem(item);
                                    }}
                                >
                                    <Icon type={'Entypo'} name={'dots-three-vertical'}  size={20} color={colors.primary} />
                                </TouchableOpacity>
                            </View>
                        </View>
                    )}
                    ListEmptyComponent={() => (
                        <View style={{alignItems: 'center',}} >
                            <LottieView
                                source={media.empty_lottie} 
                                autoPlay 
                                loop 
                                style={{height: screenWidth-50, width: screenWidth-50, marginBottom: 0}}
                            />

                            <Text style={{fontSize: 20, fontFamily: Poppins.Medium, color: colors.black}} >No reminders found!</Text>
                        </View>
                    )}
                />
              
            </View>

            <ActionButton onPress={() => {navigation.navigate('AddReminder')}} />

            <ReminderOptionSheet
                refRBSheet={refRBSheet}
                openDeleteModal={() => {
                    setAlertType('Delete')
                    setAlertTile("Are you sure!");
                    setAlertDescription("You want to delete?");
                    setShowAlertModal(true)
                }}
                openEditSheet={() => {navigation.navigate('EditReminder', {params: editItem})}}
            /> 

            {showAlertModal && 
                <AlertModal
                    alertType={alertType}
                    title={alertTile}
                    description={alertDescription}
                    onDelete={() => {
                        setShowAlertModal(false); 
                        deleteFunction();
                    }}
                    closeModal={() => {setShowAlertModal(false); }} 
                />
            }

        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.white,
    },
    mainContainer: {
        padding: 20,
        flex: 1,
        justifyContent: 'space-between'
    },
    title: {
        fontSize: 16,
        marginBottom: 10,
        color: colors.black,
        fontFamily: Poppins.Medium,
    },
    reminderContainer: {
        flex: 1,
        padding: 10,
        borderRadius: 10,
        marginBottom: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: colors.primary_light,
    },
    rowStyle: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    titleContainer: {
        flex: 1, 
        flexDirection: 'row', 
        alignItems: 'center',
        marginRight: 15,
    },
    reminderTitle: {
        fontSize: 18,
        color: colors.black,
        marginLeft: 8,
        marginRight: 20,
        fontFamily: Poppins.Medium,
    },
    reminderTime: {
        fontSize: 16,
        color: colors.black,
        marginRight: 8,
        marginTop: 4,
        fontFamily: Poppins.Medium,
    }
})


export default RemindersScreen