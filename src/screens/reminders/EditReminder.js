import React, {useState, useEffect} from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, BackHandler, FlatList, } from 'react-native'

import { media } from '../../global/media'
import { colors } from '../../global/colors'
import { Poppins } from '../../global/fontFamily'

import Icon from '../../utils/icons'

import Input from '../../components/input'
import PrimaryButton from '../../components/button/PrimaryButton'

import CheckBox from '@react-native-community/checkbox'
import DatePicker from 'react-native-date-picker'
import moment from 'moment'
import { getTodaysDay } from '../../global/constants'
import { useDispatch } from 'react-redux'
import { addReminderDetails, editReminderDetails } from '../../store/modules/home/actions'

const EditReminder = (props) => {

    const dispatch = useDispatch()
   

    const daysOptions = [
        'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat',
    ]

    const reminderOptions = [
        {
            title: 'Repeat',
            value: false,
        },
        {
            title: 'Play Once',
            value: false,
        },
        {
            title: 'Everyday',
            value: false,
        },
    ]


    const [errors, setErrors] = useState({});
    const [details, setDetails] = useState({})
    const [reminderName, setReminderName] = useState('');
    const [reminderType, setReminderType] = useState('Repeat');

    const [todaysDate, setTodaysDate] = useState(new Date());
    const [showDatePicker, setShowDatePicker] = useState(false);

    const [activeDay, setActiveDay] = useState('Mon');

    useEffect(() => {
        var day = getTodaysDay(todaysDate)
        setActiveDay(day);

        var details = props?.route?.params?.params
        setDetails(details)
        setReminderName(details?.reminderName)
        setActiveDay(details?.reminderDay)
        setTodaysDate(new Date(details?.reminderTime))
        setReminderType(details?.reminderType)
    }, []);


    const setReminderFunction = () => {

        var isValid = true
                
        if(!reminderName){
            console.log("Please enter a valid reminder name");
            isValid = false
            setErrors((prev) => {
                return {...prev, reminderName: 'Please enter a valid reminder name'}
            })
        }

       
        if(isValid){
            dispatch(editReminderDetails({
                id: details?.id,
                reminderName: reminderName,
                reminderDay: activeDay,
                reminderTime: (todaysDate).toString(),
                reminderType: reminderType,
            }))

            props.navigation.goBack()
        }
    }

    return (
        <SafeAreaView style={styles.container} >
            <View style={styles.mainContainer} >
                <View>
                    <Text style={styles.label} >Enter Reminder</Text>
                    <View >
                        <Input
                            value={reminderName}
                            error={errors.reminderName}
                            placeholder="Enter medicine name"
                            onChangeText={(text) => {setReminderName(text); setErrors({}); }}
                        />
                    </View>
                    
                    <Text style={[styles.label, {marginBottom: 0, marginTop: 10}]} >Select Day</Text>
                    <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}} >
                        {daysOptions?.map((item, index) => (
                            <TouchableOpacity 
                                key={index}
                                activeOpacity={0.5} 
                                onPress={() => {setActiveDay(item)}}
                                style={[styles.dayContainer, item == activeDay ? styles.activeDayContainer : null]} >
                                <Text style={[styles.dayStyle, {color: item == activeDay ? colors.white : colors.black}]} >{item}</Text>
                            </TouchableOpacity> 
                        ))}
                    </View>

                    
                    <View style={[styles.rowStyle, {borderBottomWidth: 1, marginBottom: 30, marginTop: 20, height: 50}]} >
                        <View style={{ alignItems: 'center', justifyContent: 'center', paddingTop: 10}} >
                            <Text style={[styles.label, {marginBottom: 0}]} >Set time</Text>
                        </View>
                        <TouchableOpacity
                            onPress={() => {setShowDatePicker(true)}}
                            style={{ flex: 1, alignItems: 'flex-end', padding: 10, borderRadius: 10, justifyContent: 'center'}} >
                            <Text style={styles.timeStyle} >{moment(todaysDate).format('hh:mm A')}</Text>
                        </TouchableOpacity>

                        <DatePicker
                            modal
                            date={todaysDate}
                            mode="time"
                            open={showDatePicker}
                            onConfirm={(date) => {
                                setShowDatePicker(false);
                                setTodaysDate(date);
                                
                            }}
                            onCancel={() => {
                                setShowDatePicker(false)
                            }}
                        />
                    </View>

                    <View>
                        {reminderOptions?.map((item, index) => (
                            <View style={styles.rowStyle} key={index} >
                                <Text style={styles.heading} >{item.title}</Text>
                                <CheckBox
                                    disabled={false}
                                    tintColors={{ true: colors.primary, false: colors.gray }}
                                    value={reminderType == item.title ? true : false}
                                    onValueChange={(newValue) => setReminderType(item.title)}
                                />
                            </View>
                        ))}
                    </View>

        
                </View>
                
                <PrimaryButton
                    title="Edit Reminder" 
                    onPress={() => {
                        setReminderFunction()
                    }}
                />
            </View>

           
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.white,
    },
    mainContainer: {
        padding: 20,
        flex: 1,
        justifyContent: 'space-between'
    },
    label: {
        fontSize: 16,
        marginBottom: 10,
        color: colors.black,
        fontFamily: Poppins.Medium,
    },
    heading: {
        fontSize: 18,
        marginBottom: 15,
        color: colors.black,
        fontFamily: Poppins.Medium,
    },
    rowStyle: {
        alignItems: 'center',
        justifyContent: 'space-between',
        flexDirection: 'row'
    },
    timeStyle: {
        fontSize: 22, 
        fontFamily: Poppins.Medium, 
        color: colors.black,
    },
    activeDayContainer: {
        paddingHorizontal: 10,
        borderRadius: 4,
        backgroundColor: colors.primary,
    },
    dayContainer: {
        padding: 8,
        paddingVertical: 6,
    },
    dayStyle: {
        fontSize: 14, 
        fontFamily: Poppins.Medium, 
    },
})


export default EditReminder