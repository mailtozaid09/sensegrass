import React from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, } from 'react-native'

import { media } from '../../global/media'
import { colors } from '../../global/colors'
import { Poppins } from '../../global/fontFamily'
import { screenWidth } from '../../global/constants'

import PrimaryButton from '../../components/button/PrimaryButton'
import { LocalNotification, ScheduleLocalNotification } from '../../utils/LocalPushController'
import RemotePushController from '../../utils/RemotePushController'

const OnboardingScreen = ({navigation}) => {

    return (
        <SafeAreaView style={styles.container} >
            <View style={styles.imgContainer} >
                <Image source={media.getstarted} style={{height: 220, width: 220, resizeMode: 'contain', marginTop: 20}} />
                <Text style={styles.subtitle} >Sensegrass - Building soil intelligence system for nutrients and carbon management in agriculture.</Text>
            </View>
            <PrimaryButton
                title="Get Started" 
                onPress={() => {
                    navigation.navigate('Login');
                }}
            />

            <RemotePushController />
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
        backgroundColor: colors.white,
    },
    imgContainer: {
        flex: 1, 
        alignItems: 'center', 
        justifyContent: 'center',
    },
    subtitle: {
        fontSize: 18,
        paddingHorizontal: 15,
        fontFamily: Poppins.Medium,
        color: colors.primary_dark,
        textAlign: 'center'
    }
})

export default OnboardingScreen