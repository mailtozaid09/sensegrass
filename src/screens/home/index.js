import React, {useState, useEffect} from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, BackHandler, } from 'react-native'

import { media } from '../../global/media'
import { colors } from '../../global/colors'
import { Poppins } from '../../global/fontFamily'

import FilterOptions from '../../components/custom/home/FilterOptions'
import CardDetails from '../../components/custom/home/CardDetails'
import QuickOptions from '../../components/custom/home/QuickOptions'
import ItemDetails from '../../components/custom/home/ItemDetails'

import { recent_transactions } from '../../global/sampleData';
import Clipboard from '@react-native-community/clipboard'
import Toast from '../../components/toast'
import ScanButton from '../../components/button/ScanButton'

const HomeScreen = (props) => {

    const [showToast, setShowToast] = useState(false);

    

    useEffect(() => {
        const backAction = () => {
            BackHandler.exitApp()
            return true;
        };
    
        const backHandler = BackHandler.addEventListener(
          "hardwareBackPress",
          backAction
        );
    
        return () => backHandler.remove();
      }, []);
    
    const showToastMsg = (upi_id) => {
        Clipboard.setString(upi_id);
        setShowToast(true)

        setTimeout(() => {
            setShowToast(false)
        }, 1000);
    };
    
    return (
        <SafeAreaView style={styles.container} >
            <ScrollView>
                <View style={styles.mainContainer} >
                    <FilterOptions />

                    <CardDetails showToastMsg={(upi) => showToastMsg(upi)}/>

                    <QuickOptions /> 

                    <ItemDetails title='Recent Transactions' dataArr={recent_transactions} onViewAll={() => {}} />
                
                
                    <ItemDetails title='Payments Pending' dataArr={recent_transactions} onViewAll={() => {}} /> 
                
                    
                </View>

                
            </ScrollView>

            <ScanButton />

            {showToast && <Toast title="Text copied!"  />}
            
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.bg_color,
    },
    mainContainer: {
        flex: 1,
        padding: 20,
        paddingTop: 0,
        marginBottom: 70,
        alignItems: 'center',
    },
})


export default HomeScreen