import React, {useState, useEffect, useRef} from 'react'
import { Text, View, SafeAreaView, StyleSheet, Button, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, TextInput, Alert, } from 'react-native'

import { media } from '../../global/media'
import { colors } from '../../global/colors'
import { Poppins } from '../../global/fontFamily'
import { screenWidth } from '../../global/constants'

import PrimaryButton from '../../components/button/PrimaryButton'

import LottieView from 'lottie-react-native'
import OTPTextView from 'react-native-otp-textinput';
import auth from '@react-native-firebase/auth'
import AlertModal from '../../components/modal/AlertModal'
import { useDispatch } from 'react-redux'
import { userLoggedIn } from '../../store/modules/auth/actions'
import { LocalNotification } from '../../utils/LocalPushController'

const picture_url = "https://media.licdn.com/dms/image/C560BAQFmJC-qvOciIw/company-logo_200_200/0/1595668892138?e=2147483647&v=beta&t=XCqbhYFhey_6W0FBrRluQqWna3Vrj1ye6Al2QgmmHsg"

const OtpVerify = (props) => {

    const dispatch = useDispatch()
    const input = useRef(null);

    const [errors, setErrors] = useState({});
    const [otpValue, setOtpValue] = useState('');

    const [number, setNumber] = useState('');
    const [orgNumber, setOrgNumber] = useState('');

    const [confirm, setConfirm] = useState(null);

    const [alertType, setAlertType] = useState('');
    const [alertTile, setAlertTile] = useState('');
    const [alertDescription, setAlertDescription] = useState('');
    const [showAlertModal, setShowAlertModal] = useState(false);

    const [buttonLoader, setButtonLoader] = useState(false);

    useEffect(() => {
        
        var num = '+91 ' + props?.route?.params?.params
        setNumber(num)
        setOrgNumber(props?.route?.params?.params)

        signIn(num)
    }, [])

    const signIn = async (phoneNumber) => {
        try {
            const confirmation = await auth().signInWithPhoneNumber(phoneNumber);
            console.log('====================================');
            console.log("confirmation-> ",confirmation);
            console.log('====================================');
            setConfirm(confirmation);
        } catch (error) {
            setAlertType('Error')
            setAlertTile("Error!!!");
            setAlertDescription(error.code);
            setShowAlertModal(true)
            console.log("signIn error =>> ", error);
        }
    }

    const confirmVerificationCode = async (code) => {
        try {
            await confirm.confirm(code);

            LocalNotification({
                picture: picture_url,
                title: "You've successfully logged in!",
                message: `Enjoy your experience with Sensegrass.`,
            })

            setConfirm(null);
            setAlertType('Success')
            setAlertTile("Success!!!");
            setAlertDescription('User Logged In Successfully!');
            setShowAlertModal(true)

            dispatch(userLoggedIn(true))
            setButtonLoader(false)
        } catch (error) {
            setButtonLoader(false)
            if (error.code === "auth/invalid-verification-code") {
                setAlertType('Error')
                setAlertTile("Error!!!");
                setAlertDescription("Invalid Verification Code!");
                setShowAlertModal(true)
                console.log('Invalid Verification Code! => ', error);
            }else if (error.code === "auth/invalid-phone-number") {
                setAlertType('Error')
                setAlertTile("Error!!!");
                setAlertDescription("Invalid Phone Number!");
                setShowAlertModal(true)
                console.log('Invalid Verification Code! => ', error);
            }else {
                setAlertType('Error')
                setAlertTile("Error!!!");
                setAlertDescription(error.code);
                setShowAlertModal(true)
                console.log("confirm error =>> ", error);
            }
        }
      }


    const verifyFunction = () => {
        setButtonLoader(true)
        var isValid = true
                
        if(otpValue.length != 6){
            console.log("Please enter a valid OTP");
            isValid = false
            setErrors((prev) => {
                return {...prev, OTP: 'Please enter a valid OTP'}
            })
        }

        if(isValid){
            confirmVerificationCode(otpValue)
        }else{
            setButtonLoader(false)
        }


    }
    
    return (
        <SafeAreaView style={styles.container} >
            <ScrollView keyboardShouldPersistTaps >
                <View style={styles.mainContainer} >
                    <View>
                        <LottieView
                            source={media.otp} 
                            autoPlay 
                            loop 
                            style={{height: screenWidth-70, width: screenWidth-70, }}
                        />
                    </View>

                    <View style={{marginVertical: 20,  alignItems: 'center'}} >
                        <Text style={styles.title} >OTP sent to</Text>
                        <Text style={styles.subtitle} >{number}</Text>
                    </View>
                
                    <View>

                        <View style={styles.otpContainer} >
                            <OTPTextView
                                ref={input}
                                containerStyle={styles.textInputContainer}
                                handleTextChange={(otp) => {setOtpValue(otp); setErrors({})}}
                                inputCount={6}
                                tintColor={colors.primary}
                                keyboardType="numeric"
                                textInputStyle={styles.textInputStyle}
                            />
                        
                            {errors.OTP && <Text style={{fontSize: 14, fontFamily: Poppins.Medium, color: colors.reddish }} >{errors?.OTP}</Text>}
                        </View>

                        <PrimaryButton
                            title="Verify OTP" 
                            loader={buttonLoader}
                            onPress={() => {
                                verifyFunction()
                            }}
                        />
                        <View style={styles.alreadyContainer} >
                            <Text style={styles.alreadyText} >By signing up, you agree with our Terms and conditions</Text>
                        </View>
                    </View>
                </View>
            </ScrollView>
           
            {showAlertModal && 
                <AlertModal
                    alertType={alertType}
                    title={alertTile}
                    description={alertDescription}
                    onAdd={() => {setShowAlertModal(false); props.navigation.navigate('Tabbar')}}
                    closeModal={() => {setShowAlertModal(false); }} 
                />
            }
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.white,
    },
    mainContainer: {
        padding: 20,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    alreadyContainer: {
        paddingHorizontal: 20,
        alignItems: 'center',
        justifyContent: 'center',
    },
    alreadyText: {
        fontSize: 14,
        fontFamily: Poppins.Medium,
        color: colors.gray,
        marginTop: 8,
        textAlign: 'center',
    },
    title: {
        fontSize: 16,
        fontFamily: Poppins.Medium,
        color: colors.black,
    },
    subtitle: {
        fontSize: 16,
        lineHeight: 19,
        fontFamily: Poppins.SemiBold,
        color: colors.black,
    },
    otpContainer: {
        // flexDirection: 'row',
        // alignItems: 'center',
        // justifyContent: 'space-between'
    },
    textInputStyle: {
        height: 44,
        width: 44,
        borderRadius: 6,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1,
        fontSize: 14,
        fontFamily: Poppins.Medium,
        color: colors.black,
    }
})

export default OtpVerify