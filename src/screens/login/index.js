import React, {useState, useEffect} from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, } from 'react-native'

import { colors } from '../../global/colors'
import { Poppins } from '../../global/fontFamily'

import Input from '../../components/input'
import PrimaryButton from '../../components/button/PrimaryButton'
import { media } from '../../global/media'
import LottieView from 'lottie-react-native'
import { screenWidth } from '../../global/constants'

const LoginScreen = ({navigation}) => {

    const [mobileNumber, setMobileNumber] = useState('');

    const [form, setForm] = useState({})
    const [errors, setErrors] = useState({})


    const onChange = ({name, value}) => {
        setForm({...form, [name]: value})
    }


    const loginFunction = () => {
        
        var isValid = true
                
        if(mobileNumber.length != 10){
            console.log("Please enter a valid number");
            isValid = false
            setErrors((prev) => {
                return {...prev, number: 'Please enter a valid number'}
            })
        }

        if(isValid){
            navigation.navigate('OtpVerify', {params: mobileNumber});
        }


    }
    
    return (
        <SafeAreaView style={styles.container} >
            <ScrollView keyboardShouldPersistTaps >
                <View style={styles.mainContainer} >
                    <View style={{marginBottom: 60}} >
                        <Image source={media.getstarted} style={{height: 220, width: 220, resizeMode: 'contain', marginTop: 20}} />
                    </View>
                
                    <View>

                        <Input
                            placeholder="Phone Number"
                            isNumber
                            error={errors.number}
                            value={mobileNumber}
                            onChangeText={(text) => {setMobileNumber(text); setErrors({}); }}
                        />

                        <PrimaryButton
                            title="Get OTP" 
                            onPress={() => {
                                loginFunction()
                            }}
                        />
                        <View style={styles.alreadyContainer} >
                            <Text style={styles.alreadyText} >By signing up, you agree with our Terms and conditions</Text>
                        </View>
                    </View>
                </View>
            </ScrollView>
           
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.white,
    },
    mainContainer: {
        padding: 20,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    alreadyContainer: {
        paddingHorizontal: 20,
        alignItems: 'center',
        justifyContent: 'center',
    },
    alreadyText: {
        fontSize: 14,
        fontFamily: Poppins.Medium,
        color: colors.gray,
        marginTop: 8,
        textAlign: 'center',
    },
    highlighted: {
        textDecorationLine: 'underline',
        color: colors.primary,
    },
})

export default LoginScreen