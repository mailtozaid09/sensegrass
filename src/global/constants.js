
import moment from "moment";
import { Dimensions } from "react-native"


export const screenWidth = Dimensions.get('window').width
export const screenHeight = Dimensions.get('window').height


export const getTodaysDay = (date) => {
    const today = moment(date); 
    const todayDay = today.day();
    //console.log(todayDay);
    var value = 'Mon';
    todayDay == 1 ? value = 'Mon' :
    todayDay == 2 ? value = 'Tue' :
    todayDay == 3 ? value = 'Wed' :
    todayDay == 4 ? value = 'Thu' :
    todayDay == 5 ? value = 'Fri' :
    todayDay == 6 ? value = 'Sat' :
    value = 'Sun'
    return value;
}


export const checkDate = (start, end) => {
    var mStart = moment(start);
    var mEnd = moment(end);
    return mStart.isBefore(mEnd);
}


export const getNumOfDays = (start, end) => {

    const date_1  =  moment(start).format('YYYY-MM-DD');
    const date_2    = moment(end).format('YYYY-MM-DD');
    
    const num_of_days = moment(date_2).diff(moment(date_1), 'days');
    
    return num_of_days
}


export const addDaysToDate = (start, days) => {
    var changeDate = moment(start).format('YYYY-MM-DD')
    var addDays = moment(changeDate).add(days, 'days');

    var formatDate = moment(addDays).format('D MMM, YYYY')
    
    return formatDate
}


