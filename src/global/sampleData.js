import { colors } from "./colors";
import { media } from "./media";

export const recent_transactions = [
  {
    "transaction_id": "1",
    "user_name": "Amit Kumar",
    "date": "2023-09-15",
    "time": "10:30 AM",
    "amount": 5000.00,
    "currency": "INR",
    "bank_name": "HDFC Bank",
    "transaction_type": "Credit",
    "description": "Salary Deposit",
    "reference_number": "TXN123456789"
  },
  {
    "transaction_id": "2",
    "user_name": "Riya Sharma",
    "date": "2023-09-16",
    "time": "02:45 PM",
    "amount": 1500.00,
    "currency": "INR",
    "bank_name": "ICICI Bank",
    "transaction_type": "Debit",
    "description": "Online Shopping",
    "reference_number": "TXN987654321"
  },
  {
    "transaction_id": "3",
    "user_name": "Sandeep Patel",
    "date": "2023-09-17",
    "time": "09:15 AM",
    "amount": 20000.00,
    "currency": "INR",
    "bank_name": "Axis Bank",
    "transaction_type": "Credit",
    "description": "Monthly Savings",
    "reference_number": "TXN456789012"
  },
  {
    "transaction_id": "4",
    "user_name": "Neha Singh",
    "date": "2023-09-18",
    "time": "03:20 PM",
    "amount": 800.00,
    "currency": "INR",
    "bank_name": "State Bank of India",
    "transaction_type": "Debit",
    "description": "Grocery Shopping",
    "reference_number": "TXN234567890"
  },
  {
    "transaction_id": "5",
    "user_name": "Rajesh Verma",
    "date": "2023-09-19",
    "time": "11:00 AM",
    "amount": 75000.00,
    "currency": "INR",
    "bank_name": "Kotak Mahindra Bank",
    "transaction_type": "Credit",
    "description": "Investment Dividend",
    "reference_number": "TXN567890123"
  },
  {
    "transaction_id": "6",
    "user_name": "Pooja Gupta",
    "date": "2023-09-20",
    "time": "08:30 AM",
    "amount": 1200.00,
    "currency": "INR",
    "bank_name": "IDFC First Bank",
    "transaction_type": "Debit",
    "description": "Dining Out",
    "reference_number": "TXN345678901"
  },
  {
    "transaction_id": "7",
    "user_name": "Kiran Malhotra",
    "date": "2023-09-21",
    "time": "04:55 PM",
    "amount": 30000.00,
    "currency": "INR",
    "bank_name": "Punjab National Bank",
    "transaction_type": "Credit",
    "description": "Bonus Payment",
    "reference_number": "TXN678901234"
  },
  {
    "transaction_id": "8",
    "user_name": "Vikram Singh",
    "date": "2023-09-22",
    "time": "01:10 PM",
    "amount": 3500.00,
    "currency": "INR",
    "bank_name": "Canara Bank",
    "transaction_type": "Debit",
    "description": "Electronics Purchase",
    "reference_number": "TXN789012345"
  }, 
  {
    "transaction_id": "9",
    "user_name": "Deepa Choudhary",
    "date": "2023-09-23",
    "time": "12:25 PM",
    "amount": 6000.00,
    "currency": "INR",
    "bank_name": "Bank of Baroda",
    "transaction_type": "Credit",
    "description": "Freelance Payment",
    "reference_number": "TXN890123456"
  },
  {
    "transaction_id": "10",
    "user_name": "Sanjay Kapoor",
    "date": "2023-09-24",
    "time": "05:40 PM",
    "amount": 2500.00,
    "currency": "INR",
    "bank_name": "Union Bank of India",
    "transaction_type": "Debit",
    "description": "Travel Booking",
    "reference_number": "TXN901234567"
  }
  
]