const baseUrl = 'https://api.themoviedb.org/3'

const api_key = 'api_key=4d3b916ec1538ddd1e0aaaf160bda652'

const keyword = 'discover/movie'

const parameters = '&with_genres=35'

const tmdb_access_token = 'eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI0ZDNiOTE2ZWMxNTM4ZGRkMWUwYWFhZjE2MGJkYTY1MiIsInN1YiI6IjYyNDJiYWIxYzc0MGQ5MDA1ZDdhZDZkOCIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.-tMrDG6Bd6tfqtfBaHxRaaujDIBlz2vpHjdWP_HyamI'



export function getTrendingMovies() {
    return(
        fetch(
            `${baseUrl}/trending/movie/day?${api_key}`,
            {
                method: "GET",
                headers: {
                    'Content-Type': 'application/json',
                    // 'Authorization': `Bearer ${tmdb_access_token}`
                },
            }
        )
        .then(res => res.json())
    );
}

export function getAllMovies() {
    return(
        fetch(
            `${baseUrl}/trending/all/day?${api_key}`,
            {
                method: "GET",
                headers: {
                    'Content-Type': 'application/json',
                    // 'Authorization': `Bearer ${tmdb_access_token}`
                },
            }
        )
        .then(res => res.json())
    );
}



export function getAllGenreList() {
    return(
        fetch(
            `${baseUrl}/genre/movie/list?${api_key}`,
            {
              method: "GET",
            }
        )
        .then(res => res.json())
    );
}

// export function getAllMovies() {
//     return(
//         fetch(
//             `${baseUrl}/${keyword}?${api_key}`,
//             {
//               method: "GET",
//             }
//         )
//         .then(res => res.json())
//     );
// }

// export function getPopularMovies() {
//     return(
//         fetch(
//             `${baseUrl}/movie/popular?${api_key}`,
//             {
//               method: "GET",
//             }
//         )
//         .then(res => res.json())
//     );
// }

// export function getTopRatedMovies() {
//     return(
//         fetch(
//             `${baseUrl}/movie/top_rated?${api_key}`,
//             {
//               method: "GET",
//             }
//         )
//         .then(res => res.json())
//     );
// }

// export function getUpcomingMovies() {
//     return(
//         fetch(
//             `${baseUrl}/movie/upcoming?${api_key}`,
//             {
//               method: "GET",
//             }
//         )
//         .then(res => res.json())
//     );
// }


// export function getSimilarMovies(id) {
//     return(
//         fetch(
//             `${baseUrl}/movie/${id}/similar?${api_key}`,
//             {
//               method: "GET",
//             }
//         )
//         .then(res => res.json())
//     );
// }



// export function getActionMovies() {
//     return(
//         fetch(
//             `${baseUrl}/${keyword}?${api_key}&with_genres=28`,
//             {
//               method: "GET",
//             }
//         )
//         .then(res => res.json())
//     );
// }

// export function getComedyMovies() {
//     return(
//         fetch(
//             `${baseUrl}/${keyword}?${api_key}&with_genres=35`,
//             {
//               method: "GET",
//             }
//         )
//         .then(res => res.json())
//     );
// }

// export function getHorrorMovies() {
//     return(
//         fetch(
//             `${baseUrl}/${keyword}?${api_key}&with_genres=27`,
//             {
//               method: "GET",
//             }
//         )
//         .then(res => res.json())
//     );
// }
