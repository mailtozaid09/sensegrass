export const media = {
    

    getstarted: require('../../assets/images/getstarted.jpeg'),


    hi_lottie: require('../../assets/lottie/hi_lottie.json'),
    hitext_lottie: require('../../assets/lottie/hitext_lottie.json'),
    
    login: require('../../assets/lottie/login.json'),
    otp: require('../../assets/lottie/otp.json'),

    home: require('../../assets/icons/home.png'),
    home_active: require('../../assets/icons/home_active.png'),

    reminder: require('../../assets/icons/reminder.png'),
    reminder_active: require('../../assets/icons/reminder_active.png'),
    
    heart: require('../../assets/icons/heart.png'),
    heart_active: require('../../assets/icons/heart_active.png'),

    user: require('../../assets/icons/user.png'),
    user_active: require('../../assets/icons/user_active.png'),

    logout: require('../../assets/icons/logout.png'),


    login_lottie: require('../../assets/lottie/login_lottie.json'),
    dark_mode_lottie: require('../../assets/lottie/dark_mode_lottie.json'),
    empty_lottie: require('../../assets/lottie/empty_lottie.json'),
    edit_lottie: require('../../assets/lottie/edit_lottie.json'),
    error_lottie: require('../../assets/lottie/error_lottie.json'),
    error_lottie: require('../../assets/lottie/error_lottie.json'),
    logout_lottie: require('../../assets/lottie/logout_lottie.json'),
    notification_lottie: require('../../assets/lottie/notification_lottie.json'),
    success_lottie: require('../../assets/lottie/success_lottie.json'),


}
