export const colors = {
  primary: '#3FAC64',
  primary_light: '#3FAC6450',
  primary_dark: '#0E411C',

  black: '#0F0F0F',
  white: '#ffffff',


  cream: '#E8B88D',
  light_cream: '#F0E3D5',

  light_red: '#FEA7A9',

  dark_blue: '#293241',
  
  bg_color: '#fafafa',
  
  gray: '#919399',
  light_gray: '#ebecf0',
  
  blue: '#81b3f3',
  red: '#f29999',
  orange: '#f7c191',
  reddish: '#ED4545',
}