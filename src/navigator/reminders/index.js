import React from 'react';
import { SafeAreaView, Text, View, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, useColorScheme, } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';

import { colors } from '../../global/colors';
import { Poppins } from '../../global/fontFamily';

import RemindersScreen from '../../screens/reminders';
import AddReminder from '../../screens/reminders/AddReminder';
import EditReminder from '../../screens/reminders/EditReminder';

const Stack = createStackNavigator();

const RemindersStack = ({navgation}) => {

    return (
        <Stack.Navigator 
            initialRouteName="Reminders" 
        >
            <Stack.Screen
                name="Reminders"
                component={RemindersScreen}
                options={{
                    headerShown: true,
                    headerLeft: () => null,
                    headerTitle: 'Reminders',
                    headerTitleAlign: 'center',
                    headerStyle: styles.headerStyle,
                    headerTitleStyle: styles.headerTitleStyle,
                }}
            />
            <Stack.Screen
                name="AddReminder"
                component={AddReminder}
                options={{
                    headerShown: true,
                    headerTitle: 'Add Reminder',
                    headerTitleAlign: 'center',
                    headerStyle: styles.headerStyle,
                    headerTitleStyle: styles.headerTitleStyle,
                }}
            />
            <Stack.Screen
                name="EditReminder"
                component={EditReminder}
                options={{
                    headerShown: true,
                    headerTitle: 'Edit Reminder',
                    headerTitleAlign: 'center',
                    headerStyle: styles.headerStyle,
                    headerTitleStyle: styles.headerTitleStyle,
                }}
            />
        </Stack.Navigator>
    );
}

const styles = StyleSheet.create({
    headerTitleStyle: {
        fontSize: 18, 
        fontFamily: Poppins.Medium,
        color: colors.black,
    },
    headerStyle: {
        //backgroundColor: colors.primary,
    }
})

export default RemindersStack