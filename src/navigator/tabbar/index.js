import React,{useEffect, useState} from 'react'
import { Text, View, StyleSheet, ScrollView, TouchableOpacity, Image, LogBox, Platform, useColorScheme,  } from 'react-native'

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { getFocusedRouteNameFromRoute } from '@react-navigation/native';

import AsyncStorage from '@react-native-async-storage/async-storage';

import { media } from '../../global/media';
import { colors } from '../../global/colors';

import HomeStack from '../home';
import ProfileStack from '../profile';
import RemindersStack from '../reminders';


const Tab = createBottomTabNavigator();


export default function Tabbar({navigation}) {

    const [userDetails, setUserDetails] = useState({});

    useEffect(() => {

    }, [])
    

    return (
        <Tab.Navigator
            screenOptions={({ route }) => ({
                tabBarIcon: ({ focused, color, size }) => {
                let iconName, routeName, height = 24, width = 24 ;
    
                if (route.name === 'HomeStack') {
                    iconName = focused ? media.home_active : media.home
                    routeName = 'Home' 
                } 
                else if (route.name === 'RemindersStack') {
                    iconName = focused ? media.reminder_active : media.reminder
                    routeName = 'Reminder' 
                    height = 28, width = 28 ;
                }
                else if (route.name === 'ProfileStack') {
                    iconName = focused ? media.user_active : media.user
                    routeName = 'Profile'
                }
                return(
                    <View style={[{alignItems: 'center', justifyContent: 'center', height: 44, width: 44, borderRadius: 22, flexDirection: 'row' },  ]} >
                        <Image source={iconName} style={{height: height, width: width, }}/>
                    </View>
                );
                },
            })}
        >
            <Tab.Screen
                name="HomeStack"
                component={HomeStack}
                options={({navigation, route}) => ({
                    headerShown: false,
                    headerRight: () => null,
                    tabBarShowLabel: false,
                    tabBarStyle: ((route) => {
                        const routeName = getFocusedRouteNameFromRoute(route) ?? ""
                        if (routeName === 'CreateTrips' || routeName === 'EditExpense') {
                          return { display: "none",  }
                        }
                        return styles.tabbarStyle
                      })(route)
                })}
            />

            
            <Tab.Screen
                name="RemindersStack"
                component={RemindersStack}
                options={({navigation, route}) => ({
                    headerShown: false,
                    tabBarShowLabel: false,
                    tabBarStyle: ((route) => {
                        const routeName = getFocusedRouteNameFromRoute(route) ?? ""
                        if (routeName === 'AddReminder' || routeName === 'EditReminder') {
                          return { display: "none",  }
                        }
                        return styles.tabbarStyle
                      })(route)
                })}
            />       

            <Tab.Screen
                name="ProfileStack"
                component={ProfileStack}
                options={({navigation, route}) => ({
                    headerShown: false,
                    tabBarShowLabel: false,
                    tabBarStyle: ((route) => {
                        const routeName = getFocusedRouteNameFromRoute(route) ?? ""
                        if (routeName === 'AddCategory') {
                          return { display: "none",  }
                        }
                        return styles.tabbarStyle
                      })(route)
                })}
            />
        </Tab.Navigator>
    );
}



const styles = StyleSheet.create({
    tabbarStyle: {
        height: Platform.OS == 'ios' ? 90 : 70,
        paddingHorizontal: 12, 
        paddingTop: Platform.OS == 'ios' ? 15 : 0, 
        paddingBottom: Platform.OS == 'android' ? 2 : 30,
        borderTopWidth: 0.1,
        borderColor: colors.gray,
        backgroundColor: colors.white,
    }
})